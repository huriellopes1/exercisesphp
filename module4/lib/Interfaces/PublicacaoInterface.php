<?php

namespace Interfaces;

interface PublicacaoInterface
{
    /**
     * @return void
     */
    public function abrir();

    /**
     * @return void
     */
    public function fechar();

    /**
     * @return void
     */
    public function folhear();

    /**
     * @return void
     */
    public function avancarPag();

    /**
     * @return void
     */
    public function voltarPag();
}
