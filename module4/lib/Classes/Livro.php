<?php

namespace Classes;

use Exception;
use Interfaces\PublicacaoInterface;
use Classes\Pessoa;

class Livro implements PublicacaoInterface
{
    private $titulo;
    private $autor;
    private $totPaginas;
    private $pagAtual;
    private $aberto;
    private $leitor;
    private $pessoa;

    public function __construct(Pessoa $pessoa, string $titulo, string $author)
    {
        $this->titulo = $titulo;
        $this->author = $author;
        $this->pessoa = $pessoa;
    }

    /**
     * @param string $titulo
     * @return void
     */
    public function setTitulo(string $titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return void
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $autor
     * @return void
     */
    public function setAutor(string $autor)
    {
        $this->autor = $autor;
    }

    /**
     * @return void
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param integer $totPaginas
     * @return void
     */
    public function setTotPaginas(int $totPaginas)
    {
        $this->totPaginas = $totPaginas;
    }

    /**
     * @return void
     */
    public function getTotPaginas()
    {
        return $this->totPaginas;
    }

    /**
     * @param integer $pagAtual
     * @return void
     */
    public function setPagAtual(int $pagAtual)
    {
        $this->pagAtual = $pagAtual;
    }

    /**
     * @return void
     */
    public function getPagAtual()
    {
        return $this->pagAtual;
    }

    /**
     * @param string $aberto
     * @return void
     */
    public function setAberto(string $aberto)
    {
        $this->aberto = $aberto;
    }

    /**
     * @return void
     */
    public function getAberto()
    {
        return $this->aberto;
    }

    /**
     * @param string $leitor
     * @return void
     */
    public function setLeitor(string $leitor)
    {
        $this->leitor = $leitor;
    }

    /**
     * @return string
     */
    public function getLeitor()
    {
        return $this->leitor;
    }

    /**
     * @return void
     */
    public function abrir()
    {
        return 'The book has been opened.';
    }

    /**
     * @return void
     */
    public function fechar()
    {
        return 'Close book.';
    }

    /**
     * @return void
     */
    public function folhear()
    {
        return 'Flip through book.';
    }

    /**
     * @return void
     */
    public function avancarPag()
    {
        return 'Forward page.';
    }

    /**
     * @return void
     */
    public function voltarPag()
    {
        return 'Back page.';
    }
}
