<?php

namespace Classes;

use Exception;

class Pessoa
{
    private $nome;
    private $sexo;
    private $idade;

    /**
     * @param string $nome
     * @param string $sexo
     * @param integer $idade
     */
    public function __construct(string $nome, string $sexo, int $idade)
    {
        $this->nome = $nome;
        $this->sexo = $sexo;
        $this->idade = $idade;
    }

    /**
     * @param string $nome
     * @return void
     */
    public function setNome(string $nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return void
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $sexo
     * @return void
     */
    public function setSexo(string $sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return void
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param integer $idade
     * @return integer
     */
    public function setIdade(int $idade): int
    {
        $this->idade = $idade;
    }

    /**
     * @return void
     */
    public function getIdade()
    {
        return $this->idade;
    }

    public function fazerAniversario()
    {
        return 'Parabéns';
    }
}
