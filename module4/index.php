<?php

require __DIR__ . '/bootstrap.php';

use Classes\Pessoa;
use Classes\Livro;

$firstPeople = new Pessoa('Huriel Lopes', 'Masculino', 24);
$secondPeople = new Pessoa('Fulana de Tal', 'Feminino', 32);
$thirdPeople = new Pessoa('Fulaninho', 'Masculino', 34);

$firstBook = new Livro($firstPeople, 'Clean Code', 'Paulo');
echo 'First Book' . PHP_EOL . PHP_EOL;
echo $firstBook->abrir() . PHP_EOL;
echo $firstBook->fechar() . PHP_EOL;
echo $firstBook->folhear() . PHP_EOL;
echo $firstBook->avancarPag() . PHP_EOL;
echo $firstBook->voltarPag() . PHP_EOL . PHP_EOL;

$secondBook = new Livro($secondPeople, 'Clean Architeture', 'Gabriel');
echo 'Second Book.' . PHP_EOL . PHP_EOL;
echo $secondBook->abrir() . PHP_EOL;
echo $secondBook->fechar() . PHP_EOL;
echo $secondBook->folhear() . PHP_EOL;
echo $secondBook->avancarPag() . PHP_EOL;
echo $secondBook->voltarPag() . PHP_EOL . PHP_EOL;

$thirdBook = new Livro($thirdPeople, 'Clean Architeture', 'Luiz');
echo 'Third Book.' . PHP_EOL . PHP_EOL;
echo $thirdBook->abrir() . PHP_EOL;
echo $thirdBook->fechar() . PHP_EOL;
echo $thirdBook->folhear() . PHP_EOL;
echo $thirdBook->avancarPag() . PHP_EOL;
echo $thirdBook->voltarPag() . PHP_EOL . PHP_EOL;

$fourthBook = new Livro($thirdPeople, 'Domain Drive Design', 'João');
echo 'Fourth Book. ' . $fourthBook->getTitulo() . PHP_EOL . PHP_EOL;
echo $fourthBook->abrir() . PHP_EOL;
echo $fourthBook->fechar() . PHP_EOL;
echo $fourthBook->folhear() . PHP_EOL;
echo $fourthBook->avancarPag() . PHP_EOL;
echo $fourthBook->voltarPag() . PHP_EOL;
