<?php

function AverageNumber()
{
    $sum = 0;
    $i = 0;
    $numbers = func_get_args();

    while ($numbers != 0) {
        $sum = $numbers + $numbers;

        $i++;
    }

    $media = $sum / $i;

    return $media;
}

$number = readline('Digite os número ex.:1,2,3 : ');

$numbers = array_map('intval', explode(',', $number));

$average = AverageNumber($numbers);

echo "A média entre os números é: " . $average . PHP_EOL;
