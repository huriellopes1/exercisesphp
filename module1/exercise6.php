<?php

function DayWeek() {
    $week = readline("Digite um número: ");

    switch($week) {
        case 1:
            echo 'Segunda-Feira'.PHP_EOL;
            break;
        case 2:
            echo 'Terça-Feira'.PHP_EOL;
            break;
        case 3:
            echo 'Quarta-Feira'.PHP_EOL;
            break;
        case 4:
            echo 'Quinta-Feira'.PHP_EOL;
            break;
        case 5:
            echo 'Sexta-Feira'.PHP_EOL;
            break;
        case 6:
            echo 'Sabádo'.PHP_EOL;
            break;
        case 7:
            echo 'Domingo'.PHP_EOL;
            break;
        default:
            echo 'Valor inválido!'.PHP_EOL;
            break;
    }
}

DayWeek();