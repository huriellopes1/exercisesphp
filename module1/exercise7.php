<?php

function Triangle() {
    $oneSide = readline('Digite o valor do primeiro lado do triângulo: ');

    $twoSide = readline('Digite o valor do segundo lado do triângulo: ');

    $threeSide = readline('Digite o valor do terceiro lado do triângulo: ');

    if ($oneSide + $twoSide > $threeSide && $oneSide + $threeSide > $twoSide && $twoSide + $threeSide > $oneSide) {
        echo 'Os três lados formam um triângulo!' . PHP_EOL;

        if ($oneSide == $twoSide && $oneSide == $threeSide) {
            echo 'Triângulo Equilátero: três lados iguais!' . PHP_EOL;
        } elseif ($oneSide == $twoSide || $oneSide == $threeSide || $twoSide == $threeSide) {
            echo 'Triângulo Isósceles: quaisquer dois lados iguais' . PHP_EOL;
        } else {
            echo 'Triângulo Escaleno: três lados diferentes' . PHP_EOL;
        }
    } else {
        echo 'Os 3 lados não formam um triângulo!' . PHP_EOL;
    }
}

Triangle();