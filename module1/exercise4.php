<?php

function convertMetersToCentimeters() {
    $meters = readline("Digite o número em metros: ");

    return $meters * 100;
}

$result = convertMetersToCentimeters();

echo "O valor informado em metros, para centimentros: " . $result  .'cm'. PHP_EOL;